package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;



public class UserDAO implements CRUD{
		
	PreparedStatement ps;
	ResultSet rs;
	int res ;
	String msj;
	Connection con;
	Conexion conex = new Conexion();
	
		
	@Override
	public List<User> listar() {
		
	
		List<User> datos = new ArrayList<>();
		String sql = "select * from user";
		
		try {
			
			con  = conex.getConnection();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				User u = new User();
			u.setId(rs.getInt(1));
			u.setFname(rs.getString("fname"));
			u.setLname(rs.getString("lname"));
				
			datos.add(u);
			}
			
			rs.close();
			ps.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace(System.out);
			System.out.println("no entro");
		}
		
		
		return datos;
	}


	
	@Override
	public User listarID(int id) {
		String sql = "SELECT * FROM user WHERE id="+id;
		
		User user = new User();
		
		try {

			con  = conex.getConnection();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
			user.setId(rs.getInt(1));
			user.setFname(rs.getString("fname"));
			user.setLname(rs.getString("lname"));
				
			
			}

			rs.close();
			ps.close();
			con.close();
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		return user;
	}

	@Override
	public String add(String nombre, String ape) {
		String sql = "insert into user(fname, lname) values(?,?) ";
		
		try {
			con = conex.getConnection();
			ps=con.prepareStatement(sql);
			ps.setString(1, nombre);
			ps.setString(2, ape);
			res = ps.executeUpdate();
			
			if (res == 1) {
				msj = "Usuario Agregado";
			}else {
				msj = "Error";
			}
			

			rs.close();
			ps.close();
			con.close();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		return msj;
	}

	@Override
	public String edit(int id, String nombre, String ape) {
		String sql="UPDATE user set fname=?,  lname=? where id=?";
		 
		try {
			con = conex.getConnection();
			ps=con.prepareStatement(sql);
			ps.setString(1, nombre);
			ps.setString(2, ape);
			ps.setInt(3, id);
			res = ps.executeUpdate();
			
			if (res == 1) {
				msj = "Usuario Actualizado";
			}else {
				msj = "Error";
			}
			

			rs.close();
			ps.close();
			con.close();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		return msj;
	}

	@Override
	public String delete(int id) {
		String sql = "DELETE FROM user WHERE id = ?";
		try {
			con = conex.getConnection();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			res = ps.executeUpdate();
			if (res == 1) {
				msj="valor eliminado";
			}else {
				msj = "error";
			}
			

			rs.close();
			ps.close();
			con.close();
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return msj;
	}
}
