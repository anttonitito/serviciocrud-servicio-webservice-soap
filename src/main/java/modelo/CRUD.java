package modelo;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;


public interface CRUD {
	

	public List<User> listar();
	
	public User listarID(int id);
	public String add(String nombre, String ape);
	public String edit(int id, String nombre, String ape);
	public String delete(int id);
	
}
