package webservice;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import modelo.User;
import modelo.UserDAO;


@WebService(serviceName = "webservice.Servicios" )
public class Servicios {

		UserDAO dao = new UserDAO();
		
		
		@WebMethod(operationName = "listar")
		public List<User> listar(){
			List datos = dao.listar();
			return datos;
		}
		
		
		@WebMethod(operationName = "agregar")
		public String agregar(@WebParam(name = "nombres") String nombres, @WebParam(name = "apellidos") String apellidos ) {
	
			String datos = dao.add(nombres, apellidos);
			return datos;
		}
		
		
		@WebMethod(operationName = "listarID")
		public User listarID(@WebParam(name = "id") int id ) {
			
			User dato = dao.listarID(id);
			return dato;
		}
		
		
		@WebMethod(operationName = "editar")
		public String editar(@WebParam(name = "id") int id, @WebParam(name = "nombres") String nombres, @WebParam(name = "apellidos") String apellidos) {
			
			String msj = dao.edit(id, nombres, apellidos);
			return msj;
		}
		
		
		@WebMethod(operationName = "eliminar")
		public String eliminar(@WebParam(name = "id") int id) {
			
			String datos = dao.delete(id);
			return datos;
		}
		
	
}
