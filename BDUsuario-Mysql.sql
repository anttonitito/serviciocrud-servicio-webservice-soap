create database prueba;
use prueba;

drop table user;
create table user(
id int primary key auto_increment not null,
fname varchar(300),
lname varchar(300)
);


select * from user;

insert into user(fname,lname) values("Patsy", "Pinto");
insert into user(fname,lname) values("Antoni", "Tito");
insert into user(fname,lname) values("Cañita", "Bronco");
