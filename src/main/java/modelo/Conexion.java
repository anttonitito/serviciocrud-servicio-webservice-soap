package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

public class Conexion {


	private static String URL = "jdbc:mysql://localhost:3306/prueba?useSSL=false&useTimezone=true";
	private static String USER = "root";
	private static String PASS = "admin";
	
	private static BasicDataSource dataSource;
	
	
	private static DataSource getDataSource() {
		
		if(dataSource == null) {
		 dataSource = new BasicDataSource();
		 dataSource.setUrl(URL);
		 dataSource.setUsername(USER);
		 dataSource.setPassword(PASS);
		 dataSource.setInitialSize(50);
		}
		
		return dataSource;
		
	}
	
	
	public Connection getConnection() throws SQLException {
		System.out.println("conecto");
		return getDataSource().getConnection();
	}
	
	
	
}