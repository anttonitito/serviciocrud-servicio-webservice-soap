package modelo;

public class User {

	
	 int id;
	 String fname;
	 String lname;

	
	
	public User() {
	}

	public User(int id, String fname, String lname) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [id=").append(id).append(", fname=").append(fname).append(", lname=").append(lname)
				.append("]");
		return builder.toString();
	}
	
	
}
