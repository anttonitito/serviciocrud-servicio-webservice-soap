package test;

import java.util.List;

import javax.xml.ws.Endpoint;

import modelo.User;
import modelo.UserDAO;
import webservice.Servicios;

public class test {
	public static void main(String[] args) {
		
		Servicios servicios = new Servicios(); 
		
		Endpoint.publish("http://localhost:3030/WS/crud", servicios);
		
		
		
		UserDAO crud = new UserDAO();
		List<User> usuarios = crud.listar();
		
		for (User user : usuarios) {
			System.out.println("persona: " + user);
		}
	}
	
}
